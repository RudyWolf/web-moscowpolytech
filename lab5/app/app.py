from flask import Flask, request, render_template, session, redirect, url_for
app = Flask(__name__)
application = app

app.secret_key = b'YidceGQxNFx4OTVceGQ0XHhjZSJceGI5XHgwMioxXHhlN1x4YzUrXHhiNS8rJwo='

@app.route('/')
def sessions():
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    err_msg = ''
    if request.method == 'POST':
        if request.form['username'] == 'admin' and request.form['password'] == '1234':
            session['username'] = request.form['username']
        return render_template('index.html')
    else:
        err_msg = 'Неверный логин и/или пароль'
    return render_template('login.html', err_msg=err_msg)


@app.route('/logout')
def logout():
    session.pop('username', None)
    return render_template('index.html')